// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.

// sum = 0
// count = R0
// multiplier = R1

// for (count = R0; count--; count > 0) {
//     sum = sum + multiplier
// }

// R2 = sum

	@sum
	M = 0

	@R0
	D = M
	@count
	M = D

	@R1
	D = M
	@multiplier
	M = D

(LOOP)
	@count
	D = M
	@SUM
	D; JEQ

	@sum
	D = M
	@multiplier
	D = D + M
	@sum
	M = D

	@count
	M = M - 1
	@LOOP
	0; JMP

(SUM)
	@sum
	D = M
	@R2
	M = D

(END)
	@END
	0; JMP

// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input. 
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel. When no key is pressed, the
// program clears the screen, i.e. writes "white" in every pixel.

// Put your code here.

(BEGIN)
@KBD
D = M
@SETVALUE
D; JEQ

D = -1

(SETVALUE)
@value
M = D

@SCREEN
D = D - M
@BEGIN
D; JEQ

// for (i = 0; i++; i < n) {
//     SCREEN[i] = value
// }

	// Suppose that n = 8192

	// n = 8192
	@8192
	D = A
	@n
	M = D

	// initialize i = 0
	@i
	M = 0

(LOOP)
	// if (i==n) goto BEGIN
	@i
	D = M
	@n
	D = D - M
	@BEGIN
	D; JEQ

	// RAM[SCREEN + i] = value
	@SCREEN
	D = A
	@i
	D = D + M
	@ptr
	M = D
	@value
	D = M
	@ptr
	A = M
	M = D

	// i++
	@i
	M = M + 1

	@LOOP
	0; JMP
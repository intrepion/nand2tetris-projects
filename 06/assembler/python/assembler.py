from sys import argv

assemblyFilename = argv[1]
period = assemblyFilename.rfind('.')
programName = assemblyFilename[:period]
extension = assemblyFilename[period:]
if '.asm' != extension:
    raise ValueError('Expecting an .asm file but got a ' + extension + ' file')
symbols = dict(
    {
        'sp':     0,
        'lcl':    1,
        'arg':    2,
        'this':   3,
        'that':   4,
        'r0':     0,
        'r1':     1,
        'r2':     2,
        'r3':     3,
        'r4':     4,
        'r5':     5,
        'r6':     6,
        'r7':     7,
        'r8':     8,
        'r9':     9,
        'r10':    10,
        'r11':    11,
        'r12':    12,
        'r13':    13,
        'r14':    14,
        'r15':    15,
        'screen': 16384,
        'kbd':    24576,
    }
)
computeBinary = dict(
    {
        '0':   '0101010',
        '1':   '0111111',
        '-1':  '0111010',
        'd':   '0001100',
        'a':   '0110000',
        '!d':  '0001101',
        '!a':  '0110001',
        '-d':  '0001111',
        '-a':  '0110011',
        'd+1': '0011111',
        'a+1': '0110111',
        'd-1': '0001110',
        'a-1': '0110010',
        'd+a': '0000010',
        'd-a': '0010011',
        'a-d': '0000111',
        'd&a': '0000000',
        'd|a': '0010101',
        'm':   '1110000',
        '!m':  '1110001',
        '-m':  '1110011',
        'm+1': '1110111',
        'm-1': '1110010',
        'd+m': '1000010',
        'd-m': '1010011',
        'm-d': '1000111',
        'd&m': '1000000',
        'd|m': '1010101',
    }
)
destinationBinary = dict(
    {
        '':    '000',
        'm':   '001',
        'd':   '010',
        'md':  '011',
        'a':   '100',
        'am':  '101',
        'ad':  '110',
        'amd': '111',
    }
)
jumpBinary = dict(
    {
        '':    '000',
        'jgt': '001',
        'jeq': '010',
        'jge': '011',
        'jlt': '100',
        'jne': '101',
        'jle': '110',
        'jmp': '111',
    }
)
def decimalTo16Bit(decimal):
    decimal %= 65536
    binary = bin(65536 + decimal)[-16:]
    return binary
lines = []
programCounter = 0
with open(assemblyFilename, 'r') as filePointer:
    for line in filePointer:
        if '//' in line:
            comment = line.find('//')
            line = line[:comment]
        line = line.strip().replace(' ', '').lower()
        if 0 != len(line):
            if '(' == line[:1]:
                newSymbol = line[1:-1]
                if newSymbol in symbols:
                    raise KeyError('symbol ' + newSymbol + ' already defined!')
                symbols[newSymbol] = programCounter
            else:
                lines.append(line)
                programCounter += 1
hackFilename = programName + '.hack'
variableLocation = 16
with open(hackFilename, 'w') as filePointer:
    for line in lines:
        assembly = ''
        if '@' == line[:1]:
            key = line[1:]
            if key.isdigit():
                assembly = decimalTo16Bit(int(key))
            elif key in symbols:
                assembly = decimalTo16Bit(symbols[key])
            elif key not in symbols:
                symbols[key] = variableLocation
                assembly = decimalTo16Bit(variableLocation)
                variableLocation += 1
            else:
                raise KeyError('symbol ' + key + ' unexpected!')
        else:
            destination = ''
            compute = line
            jump = ''
            if '=' in compute:
                equals = line.find('=')
                destination = line[:equals]
                compute = line[(equals+1):]
            if ';' in compute:
                semicolon = line.find(';')
                compute = line[:semicolon]
                jump = line[(semicolon+1):]
            assembly = '111'
            assembly += computeBinary[compute]
            assembly += destinationBinary[destination]
            assembly += jumpBinary[jump]
        filePointer.write(assembly + '\n')
package main

import (
    "bufio"
    "fmt"
    "os"
)

func main() {
    file, err := os.Open(os.Args[1])

    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }

    defer file.Close()

    reader := bufio.NewReader(file)
    scanner := bufio.NewScanner(reader)

    for scanner.Scan() {
        fmt.Println(scanner.Text())
    }

}